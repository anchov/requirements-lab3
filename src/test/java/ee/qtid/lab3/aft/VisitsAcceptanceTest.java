package ee.qtid.lab3.aft;

import com.codeborne.selenide.Condition;
import ee.qtid.lab3.page.BasePage;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static org.junit.Assert.assertTrue;

public class VisitsAcceptanceTest extends BasePage {

    @Test
    public void AFT03_visitCanCanBeArrangedForPet() {
        navigateToOwner();
        createVisit();
    }

    private void navigateToOwner() {
        BasePage.navigateTo();
        sleep(1000);
        BasePage.openFindOwnersTab();
        sleep(1000);
        $(byText("Find Owner")).click();
        sleep(1000);
        $(byText("George Franklin")).click();
        sleep(1000);
    }

    private void createVisit() {
        $(byText("Add Visit")).waitUntil(Condition.visible, 3000).click();
        String description = RandomStringUtils.randomAlphabetic(15);
        $(byId("description")).waitUntil(Condition.visible, 3000).setValue(description);
        $(byId("date")).setValue(LocalDate.now().plusDays(3).format(DateTimeFormatter.ISO_DATE));
        $(byText("Add Visit")).click();
        assertTrue($(byText(description)).waitUntil(Condition.visible, 3000).isDisplayed());
    }
}
