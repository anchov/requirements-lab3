package ee.qtid.lab3.ft.owner.create;

import com.codeborne.selenide.CollectionCondition;
import ee.qtid.lab3.page.BasePage;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static ee.qtid.lab3.page.OwnerPageHelper.openOwnersAndFillOwnerCreation;
import static org.junit.Assert.assertTrue;

public class OwnerCreationTest extends BasePage {

    @Test
    public void FT04_whenUserNavigatesToAddOwnerPageAndSubmitsValidDataThenNewOwnerIsCreated() {
        openOwnersAndFillOwnerCreation();
        $(By.className("btn")).click();
        assertTrue($(byText("Owner Information")).isDisplayed());
    }

    @Test
    public void FT05_whenUserNavigatesToAddOwnerPageAndSubmitsWithEmptyPhoneThenErrorIsDisplayed() {
        openOwnersAndFillOwnerCreation();
        $(By.id("telephone")).setValue("");
        $(By.className("btn")).click();
        $$(byText("must not be empty")).shouldHave(CollectionCondition.size(1));
    }

    @Test
    public void FT06_whenUserNavigatesToAddOwnerPageAndSubmitsWithEmptyFirstNameThenErrorIsDisplayed() {
        openOwnersAndFillOwnerCreation();
        $(By.id("firstName")).setValue("");
        $(By.className("btn")).click();
        $$(byText("must not be empty")).shouldHave(CollectionCondition.size(1));
    }

    @Test
    public void FT07_whenUserNavigatesToAddOwnerPageAndSubmitsWithEmptyLastNameThenErrorIsDisplayed() {
        openOwnersAndFillOwnerCreation();
        $(By.id("lastName")).setValue("");
        $(By.className("btn")).click();
        $$(byText("must not be empty")).shouldHave(CollectionCondition.size(1));
    }

    @Test
    public void FT08_whenUserNavigatesToAddOwnerPageAndSubmitsWithEmptyAddressThenErrorIsDisplayed() {
        openOwnersAndFillOwnerCreation();
        $(By.id("address")).setValue("");
        $(By.className("btn")).click();
        $$(byText("must not be empty")).shouldHave(CollectionCondition.size(1));
    }

    @Test
    public void FT09_whenUserNavigatesToAddOwnerPageAndSubmitsWithEmptyCityThenErrorIsDisplayed() {
        openOwnersAndFillOwnerCreation();
        $(By.id("city")).setValue("");
        $(By.className("btn")).click();
        $$(byText("must not be empty")).shouldHave(CollectionCondition.size(1));
    }

    @Test
    public void FT10_whenUserNavigatesToAddOwnerPageAndSubmitsWithInvalidPhoneLengthThenErrorIsDisplayed() {
        openOwnersAndFillOwnerCreation();
        $(By.id("telephone")).setValue(RandomStringUtils.randomNumeric(11));
        $(By.className("btn")).click();
        $$(byText("numeric value out of bounds (<10 digits>.<0 digits> expected)")).shouldHave(CollectionCondition.size(1));
    }
}
