package ee.qtid.lab3.aft;

import com.codeborne.selenide.Condition;
import ee.qtid.lab3.page.BasePage;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static ee.qtid.lab3.page.OwnerPageHelper.openOwnersAndFillOwnerCreation;
import static org.junit.Assert.assertTrue;

public class OwnersAcceptanceTest extends BasePage {

    @Test
    public void AFT01_ownerCanBeCreatedAndThenEdited() {
        createOwner();
        editOwner();
    }

    private void createOwner() {
        openOwnersAndFillOwnerCreation();
        $(By.className("btn")).click();
        assertTrue($(byText("Owner Information")).isDisplayed());
    }

    private void editOwner() {
        $(byText("Edit Owner")).click();
        String newCity = RandomStringUtils.randomAlphabetic(5);
        $(By.id("city")).waitUntil(Condition.visible, 3000).setValue(newCity);
        $(By.className("btn")).click();
        assertTrue($(byText("Owner Information")).waitUntil(Condition.visible, 3000).isDisplayed());
        assertTrue($(byText(newCity)).isDisplayed());
    }
}
