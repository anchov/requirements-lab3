package ee.qtid.lab3.ft;

import com.codeborne.selenide.Condition;
import ee.qtid.lab3.page.BasePage;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static org.junit.Assert.assertTrue;

public class PetTest {

    @Before
    public void setup() {
        BasePage.navigateTo();
        sleep(1000);
        BasePage.openFindOwnersTab();
        sleep(1000);
        $(byText("Find Owner")).click();
        sleep(1000);
        $(byText("George Franklin")).click();
    }

    @Test
    public void FT14_testEditExistingPet() {
        sleep(1000);
        $(byXpath("/html/body/div/div/table[2]/tbody/tr[1]/td[2]/table/tbody/tr/td[1]/a")).waitUntil(Condition.visible, 3000).click();
        String newName = RandomStringUtils.randomAlphabetic(10);
        $(byName("name")).waitUntil(Condition.visible, 3000).setValue(newName);
        $(byName("birthDate")).setValue("1996-03-21");
        $(byName("type")).selectOption("snake");
        $(byText("Update Pet")).waitUntil(Condition.visible, 3000).click();
        assertTrue($(byText(newName)).waitUntil(Condition.visible, 3000).isDisplayed());
    }

    @Test
    public void FT15_testAddNewValidPet() {
        sleep(1000);
        $(byText("Add New Pet")).waitUntil(Condition.visible, 3000).click();
        String petName = RandomStringUtils.randomAlphabetic(10);
        $(byName("name")).waitUntil(Condition.visible, 3000).setValue(petName);
        $(byName("birthDate")).setValue("1996-03-21");
        $(byText("Add Pet")).click();
        assertTrue($(byText(petName)).waitUntil(Condition.visible, 3000).isDisplayed());
    }

    @Test
    public void FT16_testAddNewPetEmptyBirthDate() {
        sleep(1000);
        $(byText("Add New Pet")).waitUntil(Condition.visible, 3000).click();
        String petName = RandomStringUtils.randomAlphabetic(10);
        $(byName("name")).waitUntil(Condition.visible, 3000).setValue(petName);
        $(byName("birthDate")).setValue("");
        $(byText("Add Pet")).click();
        assertTrue($(byText(petName)).waitUntil(Condition.visible, 3000).isDisplayed());
    }

    @Test
    public void FT17_testAddNewPetEmptyName() {
        sleep(1000);
        $(byText("Add New Pet")).waitUntil(Condition.visible, 3000).click();
        $(byName("name")).waitUntil(Condition.visible, 3000).setValue("");
        $(byName("birthDate")).setValue("1996-03-21");
        $(byText("Add Pet")).click();
        assertTrue($(byText("is required")).waitUntil(Condition.visible, 3000).isDisplayed());
    }

    @Test
    public void FT18_testAddNewPetDifferentSpecies() {
        sleep(1000);
        for (String newType : Arrays.asList("bird", "cat", "dog", "hamster", "lizard", "snake")) {
            sleep(1000);
            $(byText("Add New Pet")).waitUntil(Condition.visible, 3000).click();
            String petName = RandomStringUtils.randomAlphabetic(10);
            sleep(1000);
            $(byName("name")).waitUntil(Condition.visible, 3000).setValue(petName);
            $(byName("birthDate")).setValue("1996-03-21");
            $(byName("type")).selectOption(newType);
            sleep(1000);
            $(byText("Add Pet")).click();
            sleep(1000);
            assertTrue($(byText(petName)).waitUntil(Condition.visible, 3000).isDisplayed());
        }
    }

    @Test
    public void FT19_testAddNewValidVisit() {
        sleep(1000);
        $(byXpath("/html/body/div/div/table[2]/tbody/tr[1]/td[2]/table/tbody/tr/td[2]/a")).waitUntil(Condition.visible, 3000).click();
        $(byName("date")).waitUntil(Condition.visible, 3000).setValue("2050-11-24");
        $(byName("description")).setValue("test description");
        $(byText("Add Visit")).click();
        assertTrue($(byText("test description")).waitUntil(Condition.visible, 3000).isDisplayed());
    }

    @Test
    public void FT20_testAddPetInvalidBirthDate() {
        sleep(1000);
        $(byText("Add New Pet")).waitUntil(Condition.visible, 3000).click();
        String petName = RandomStringUtils.randomAlphabetic(10);
        $(byName("name")).waitUntil(Condition.visible, 3000).setValue(petName);
        $(byName("birthDate")).setValue("1996/03/21");
        $(byText("Add Pet")).click();
        assertTrue($(byText("invalid date. Must use pattern yyyy-MM-dd")).waitUntil(Condition.visible, 3000).isDisplayed());
    }

}
