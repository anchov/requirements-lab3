package ee.qtid.lab3.page;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selectors.byTitle;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class BasePage {

    public static final String BASE_PAGE_URL = "https://softqual-petclinic.herokuapp.com/";

    private static By homeTab = byTitle("home page");
    private static By findOwnersTab = byTitle("find owners");
    private static By veterinariansTab = byTitle("veterinarians");
    private static By errorTab = byTitle("trigger a RuntimeException to see how it is handled");
    private static By welcomeText = byText("Welcome");
    private static By veterinariansText = byText("Veterinarians");
    private static By findOwnersText = byText("Find Owners");
    private static By errorText = byText("Something happened...");

    public static void navigateTo() {
        open(BASE_PAGE_URL);
    }

    public static boolean isBasePage() {
        return $(welcomeText).isDisplayed();
    }

    public static boolean isVeterinariansTab() {
        return $(veterinariansText).isDisplayed();
    }

    public static boolean isFindOwnersTab() {
        return $(findOwnersText).isDisplayed();
    }

    public static boolean isErrorTab() {
        return $(errorText).isDisplayed();
    }

    public static void openHomeTab() {
        $(homeTab).click();
    }

    public static void openFindOwnersTab() {
        $(findOwnersTab).click();
    }

    public static void openVeterinariansTab() {
        $(veterinariansTab).click();
    }

    public static void openErrorTab() {
        $(errorTab).click();
    }

}
