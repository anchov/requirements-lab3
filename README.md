#Lab3 for Tartu Qtid

To run tests:
* clone repository
* run `./gradlew build`
* run tests in `src/test/java/ee/qtid/lab3`

Different tests are provided in separate Java `packages`:
* `ee.qtid.lab3.aft` - contains acceptance functional tests
* `ee.qtid.lab3.ft` - contains functional tests

The `Selenide` (Selenium) tests use `Chrome-driver` as default, so `Google Chrome` is required.