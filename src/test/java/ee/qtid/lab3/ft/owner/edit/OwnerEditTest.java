package ee.qtid.lab3.ft.owner.edit;

import ee.qtid.lab3.page.BasePage;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static org.junit.Assert.assertTrue;

public class OwnerEditTest extends BasePage {

    private static String ownersEditPage = BASE_PAGE_URL + "owners/%s";

    @Test
    public void FT11_whenUserNavigatesToEditOwnerPageAndSubmitsValidDataThenOwnerIsUpdated() {
        int ownerIdToEdit = 2;
        String ownerPageUrl = String.format(ownersEditPage, ownerIdToEdit);
        open(ownerPageUrl);
        $(byText("Edit Owner")).click();
        String newCity = RandomStringUtils.randomAlphabetic(5);
        $(By.id("city")).setValue(newCity);
        $(By.className("btn")).click();
        assertTrue($(byText("Owner Information")).isDisplayed());
        assertTrue($(byText(newCity)).isDisplayed());
    }
}