package ee.qtid.lab3.page;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static ee.qtid.lab3.page.BasePage.BASE_PAGE_URL;

import com.codeborne.selenide.Selenide;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;

public class OwnerPageHelper {

  public static String ownersPage = BASE_PAGE_URL + "owners/find";

  public static void openOwnersPage() {
    open(ownersPage);
  }

  public static void openOwnersAndFillOwnerCreation() {
    openOwnersPage();
    $("a[href=\"/owners/new\"]").waitUntil(visible, 5000);
    $("a[href=\"/owners/new\"]").click();
    Selenide.sleep(500);
    $(By.id("firstName")).waitUntil(visible, 5000);
    $(By.id("firstName")).setValue(RandomStringUtils.randomAlphabetic(10));
    $(By.id("lastName")).setValue(RandomStringUtils.randomAlphabetic(10));
    $(By.id("address")).setValue(RandomStringUtils.randomAlphabetic(10));
    $(By.id("city")).setValue(RandomStringUtils.randomAlphabetic(5));
    $(By.id("telephone")).setValue(RandomStringUtils.randomNumeric(7));
  }
}
