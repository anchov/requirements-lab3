package ee.qtid.lab3.aft;

import com.codeborne.selenide.Condition;
import ee.qtid.lab3.page.BasePage;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.byName;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static org.junit.Assert.assertTrue;

public class PetsAcceptanceTest extends BasePage {

    @Test
    public void AFT02_ownersPetCanBeCreatedAndEdited() {
        navigateToOwner();
        createPet();
        editPet();
    }

    private void navigateToOwner() {
        BasePage.navigateTo();
        sleep(1000);
        BasePage.openFindOwnersTab();
        sleep(1000);
        $(byText("Find Owner")).click();
        sleep(1000);
        $(byText("George Franklin")).click();
        sleep(1000);
    }

    private void createPet() {
        $(byText("Add New Pet")).waitUntil(Condition.visible, 5000).click();
        String petName = RandomStringUtils.randomAlphabetic(10);
        $(byName("name")).waitUntil(Condition.visible, 3000).setValue(petName);
        $(byName("birthDate")).setValue("1996-03-21");
        $(byText("Add Pet")).click();
        assertTrue($(byText(petName)).waitUntil(Condition.visible, 3000).isDisplayed());
    }

    private void editPet() {
        sleep(1000);
        $(byText("Edit Pet")).click();
        String newName = RandomStringUtils.randomAlphabetic(10);
        $(By.id("name")).waitUntil(Condition.visible, 3000).setValue(newName);
        $(By.className("btn")).click();
        assertTrue($(byText("Owner Information")).waitUntil(Condition.visible, 3000).isDisplayed());
        assertTrue($(byText(newName)).isDisplayed());
    }
}
