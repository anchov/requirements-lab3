package ee.qtid.lab3.ft.owner.find;

import static com.codeborne.selenide.CollectionCondition.size;
import static com.codeborne.selenide.CollectionCondition.texts;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static ee.qtid.lab3.page.OwnerPageHelper.openOwnersAndFillOwnerCreation;
import static ee.qtid.lab3.page.OwnerPageHelper.openOwnersPage;
import static org.junit.Assert.assertTrue;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.openqa.selenium.By;

public class FindOwnerTest {

  @Test
  public void FT02_whenTwoOwnersWithSameLastNameExistAndTheyAreQueriedByLastName_bothAreShownInTable() {
    String lastName = RandomStringUtils.randomAlphabetic(10);
    addOwner(lastName);
    addOwner(lastName);

    searchOwnerByName(lastName);

    $$(".table-striped tbody tr").shouldHave(size(2));
  }

  @Test
  public void FT03_whenOwnerLastNameIsInsertedLowercase_andItExists_userWillBeForwardedToOwnerDetailsView() {
    searchOwnerByName("franklin");

    assertOwnerInformationDisplayed("George Franklin", "110 W. Liberty St.", "Madison", "6085551023");
  }

  @Test
  public void FT12_whenOwnerLastNameIsInsertedUppercase_andItExists_userWillBeForwardedToOwnerDetailsView() {
    searchOwnerByName("FRANKLIN");

    assertOwnerInformationDisplayed("George Franklin", "110 W. Liberty St.", "Madison", "6085551023");
  }

  @Test
  public void FT13_whenOwnerLastNameIsInserted_andItDoesNotExist_hasNotBeenFoundMessageIsShown() {
    searchOwnerByName("some random owner asdasdasd");

    assertMessageHasNotBeenFound();
  }

  private void addOwner(String lastName) {
    openOwnersAndFillOwnerCreation();
    $(By.id("lastName")).setValue(lastName);
    $(By.className("btn")).click();
  }

  private void searchOwnerByName(String name) {
    openOwnersPage();
    $(By.id("lastName")).setValue(name);
    $(byText("Find Owner")).click();
  }

  private void assertOwnerInformationDisplayed(String name, String address, String city, String phone) {
    $$(".table-striped").first().shouldBe(visible).$$("tr").shouldHaveSize(4)
        .shouldHave(texts(
            "Name " + name,
            "Address " + address,
            "City " + city,
            "Telephone " + phone));
  }

  private void assertMessageHasNotBeenFound() {
    assertTrue($(By.className("help-inline")).has(text("has not been found")));
  }
}
