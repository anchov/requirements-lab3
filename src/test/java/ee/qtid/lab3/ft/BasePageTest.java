package ee.qtid.lab3.ft;

import ee.qtid.lab3.page.BasePage;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class BasePageTest {

    @Test
    public void FT01_testLandingPage() {
        BasePage.navigateTo();
        assertTrue(BasePage.isBasePage());
    }

}
